FROM debian:bullseye
RUN echo deb http://deb.debian.org/debian bullseye-backports main | tee /etc/apt/sources.list.d/bullseye-backports.list \
	&& apt-get update \
	&& apt install -y -t bullseye-backports apache2 \
	&& apt-get install -y ruby2.7 bundler \
	&& apt-get clean
ARG INGRESS_HOME=/var/lib/ingress
RUN mkdir -p $INGRESS_HOME
WORKDIR $INGRESS_HOME
COPY Gemfile Gemfile.lock ./
RUN bundle config set --local deployment 'true'
RUN bundle config set --local without 'development'
RUN bundle install --no-cache
COPY ingress/* ./
ENTRYPOINT ["bundle", "exec", "./ingress.rb"]
